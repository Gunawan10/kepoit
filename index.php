<?php 
error_reporting(0);
/*
	index.php sebagai control untuk request client
*/
//include head
include('head.php');

//include badan program berdasarkan modul yang dipanggil
$modul = htmlspecialchars($_GET['mod']);

if(isset($modul)){
	if($modul != ""){
		if($modul == "beranda"){
			include('beranda.php');
		}
		else{
			//error 404 not found
			include('error.php');
		}
	}
	else{
		include('beranda.php');
	}
}
else{
	include('beranda.php');
}

//include footer
include('footer.php');
?>